import org.joml.Vector3f;

public class Particle {
    private Vector3f position;
    private Vector3f velocity;
    private int expireTime;
    public float[] rgbAlpha = {50.0f,70.0f, 168.0f, 0.1f};
    private int lifeTime;

    public Particle(Vector3f position, Vector3f velocity, int expireTime) {
        this.position = position;
        this.velocity = velocity;
        this.expireTime = expireTime;
        this.lifeTime = expireTime;
    }

    public boolean isDestroyed() {
        return expireTime <= 0;
    }

    public void update(Vector3f gravity) {
        position.add(velocity.x, velocity.y, velocity.z);
        velocity.add(gravity.x, gravity.y, gravity.z);
        rgbAlpha[0] = expireTime / lifeTime;
        rgbAlpha[3] = expireTime / lifeTime;

        expireTime -= 1;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector3f velocity) {
        this.velocity = velocity;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        this.expireTime = expireTime;
    }

    public float[] getRgbAlpha() {
        return rgbAlpha;
    }

}