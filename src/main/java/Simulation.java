
import java.io.IOException;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.*;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryUtil;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;


public class Simulation {

    private static GLFWKeyCallback keyCallback = new GLFWKeyCallback() {

        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, true);
            }
        }
    };


    public static void main(String[] args) throws IOException, InterruptedException {

        long window;
        /* Initialize GLFW */
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        /* Create window */
        window = glfwCreateWindow(1280, 720, "Simple example", NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window");
        }

        /* Center the window on screen */
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window,
                (vidMode.width() - 1280) / 2,
                (vidMode.height() - 720) / 2
        );

        /* Create OpenGL context */
        glfwMakeContextCurrent(window);
        GL.createCapabilities();

        /* Enable vertical synchronization */
        glfwSwapInterval(1);

        /* Set the key callback */
        glfwSetKeyCallback(window, keyCallback);

        /* Declare buffers for using inside the loop */
        IntBuffer width = MemoryUtil.memAllocInt(1);
        IntBuffer height = MemoryUtil.memAllocInt(1);

        //CREATE EMMITERS

        //EMITTER 1
        List<ParticleEmitter> particleEmitters = new ArrayList<>();
        ParticleEmitter particleEmitter = new ParticleEmitter();
        particleEmitters.add(particleEmitter);

        //EMITTER 2
        ParticleEmitter particleEmitter2 = new ParticleEmitter();
        particleEmitter2.setGravity(new Vector3f(0, -0.001f, 0));
        particleEmitter2.setLocation(new Vector3f(-1f, -0.5f, 0));
        particleEmitter2.setSpawningRate(1);
        particleEmitters.add(particleEmitter2);

        //EMITTER 3
        //uses random time to live
        ParticleEmitter particleEmitter3 = new ParticleEmitter(new Vector3f(0, -1, 0));

        particleEmitters.add(particleEmitter3);

        /* Loop until window gets closed */
        while (!glfwWindowShouldClose(window)) {
            float ratio;
            /* Animate the scene */
            Thread.sleep(10);
            /* Get width and height to calcualte the ratio */
            glfwGetFramebufferSize(window, width, height);
            ratio = width.get() / (float) height.get();
            /* Rewind buffers for next get */
            width.rewind();
            height.rewind();
            /* Set viewport and clear screen */
            glViewport(0, 0, width.get(), height.get());
            glClear(GL_COLOR_BUFFER_BIT);
            /* Set ortographic projection */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-ratio, ratio, -1f, 1f, 1f, -1f);
            glMatrixMode(GL_MODELVIEW);

       //     glLoadIdentity();
        //    glRotatef((float) glfwGetTime() * 10f, (float) glfwGetTime() * 10f, (float) glfwGetTime() * 10f, (float) glfwGetTime() * 10f);


            /* TEXTURES */
            // load our texture
            glEnable(GL_TEXTURE_2D);
            int texId = Material.loadTexture(Material.loadImage("src/main/resources/snow.png"));
            glBindTexture(GL_TEXTURE_2D, texId);
            //enable transparent blend
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            float partticleSize = 0.015f;


            //draw
            glBegin(GL_QUADS);
            for (ParticleEmitter pe : particleEmitters ){
                for (Particle p : pe.getParticles()){
                    glPushMatrix();
                    glRotatef(-(float) glfwGetTime() * 10f, 1, 1, 1  );
                    float colour = (float) p.getExpireTime() / particleEmitter.getParticleLifeTime();
                    glColor4f(colour, colour, colour, colour);
                    glTexCoord2d(0.0, 0.0);
                    glVertex3f(p.getPosition().x - partticleSize, p.getPosition().y - partticleSize, p.getPosition().z);
                    glTexCoord2d(1.0, 0.0);
                    glVertex3f(p.getPosition().x + partticleSize, p.getPosition().y - partticleSize, p.getPosition().z);
                    glTexCoord2d(1.0, 1.0);
                    glVertex3f(p.getPosition().x + partticleSize, p.getPosition().y + partticleSize, p.getPosition().z);
                    glTexCoord2d(0.0, 1.0);
                    glVertex3f(p.getPosition().x - partticleSize, p.getPosition().y + partticleSize, p.getPosition().z);
                    glPopMatrix();

                }
            }

            glEnd();

            for (ParticleEmitter pe : particleEmitters){
                pe.update();
            }

            //FINISH ONE ITERATION
            /* Swap buffers and poll Events */
            glfwSwapBuffers(window);
            glfwPollEvents();
            /* Flip buffers for next loop */
            width.flip();
            height.flip();
        }

        /* Free buffers */
        MemoryUtil.memFree(width);
        MemoryUtil.memFree(height);

        /* Release window and its callbacks */
        glfwDestroyWindow(window);
        keyCallback.free();

        /* Terminate GLFW and release the error callback */
        glfwTerminate();
    }
}