import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ParticleEmitter {
    private static Random randomGenerator = new Random();
    private final List<Particle> particles;
    private Vector3f location;
    private float spawningRate;
    private int particleLifeTime;
    private Vector3f gravity;
    private Vector3f initialVelocity;
    private float velocityModifier;
    private float gravityModifier;
    private Vector3f emitterVelocity;
    private boolean useRandomLifeTime = false;

    public ParticleEmitter() {
        this(new Vector3f(-0.5f, -0.5f, 0), 5, 100, new Vector3f(0, -0.0003f, 0), new Vector3f(-0.5f, 0, -0.5f), 0.01f, 1f, new Vector3f(0.003f,0.003f,0));
    }

    public ParticleEmitter(Vector3f location){
        this(location, 5, -1, new Vector3f(0, -0.0003f, 0), new Vector3f(-0.5f, 0, -0.5f), 0.01f, 1f, new Vector3f(0.003f,0.003f,0));

    }

    public ParticleEmitter(Vector3f location, float spawningRate, int particleLifeTime, Vector3f gravity, Vector3f initialVelocity, float velocityModifier, float gravityModifier, Vector3f emitterVelocity) {
        this.location = location;
        this.spawningRate = spawningRate;
        this.particleLifeTime = particleLifeTime;
        this.gravity = gravity;
        this.initialVelocity = initialVelocity;
        this.velocityModifier = velocityModifier;
        this.gravityModifier = gravityModifier;
        this.emitterVelocity = emitterVelocity;
        if (particleLifeTime == -1){
            this.particleLifeTime = (int)randomGenerator.nextDouble() * 200;
            if (this.particleLifeTime == 0){
                this.particleLifeTime = 100;
            }
            useRandomLifeTime = true;
        }
        this.particles = new ArrayList<>((int) spawningRate * this.particleLifeTime);
    }




    private Particle generateNewParticle() {
        Vector3f particleLocation = new Vector3f(location);
        Vector3f particleVelocity = new Vector3f();
        float randomX = (float) randomGenerator.nextDouble() - 0.5f;
        float randomY = (float) randomGenerator.nextDouble() - 0.5f;
        float randomZ = (float) randomGenerator.nextDouble() - 0.5f;
        particleVelocity.x = (randomX + initialVelocity.x );
        particleVelocity.y = (randomY + initialVelocity.y);
        particleVelocity.z = (randomZ + initialVelocity.z);
        particleVelocity.mul(velocityModifier);

        return new Particle(particleLocation, particleVelocity, particleLifeTime);
    }

    public void update() {
        for (int i = 0; i < spawningRate; i++){
            Particle particle = generateNewParticle();
            if (useRandomLifeTime){
                particle.setExpireTime((int) (randomGenerator.nextDouble() * 150));
            }
            particles.add(particle);

        }
        for (int i = 0; i < particles.size(); i++) {
            Particle particle = particles.get(i);
            particle.update(new Vector3f(gravity).mul(gravityModifier));
            if (particle.isDestroyed()) {
                particles.remove(i);
                i--;
            }
        }
        this.location.add(emitterVelocity);
    }

    public Vector3f getLocation() {
        return location;
    }

    public void setLocation(Vector3f location) {
        this.location = location;
    }

    public float getSpawningRate() {
        return spawningRate;
    }

    public void setSpawningRate(float spawningRate) {
        this.spawningRate = spawningRate;
    }

    public Vector3f getGravity() {
        return gravity;
    }

    public void setGravity(Vector3f gravity) {
        this.gravity = gravity;
    }

    public int getParticleLifeTime() {
        return particleLifeTime;
    }

    public void setParticleLifeTime(int particleLifeTime) {
        this.particleLifeTime = particleLifeTime;
    }

    public Vector3f getInitialVelocity() {
        return initialVelocity;
    }

    public void setInitialVelocity(Vector3f initialVelocity) {
        this.initialVelocity = initialVelocity;
    }
    public List<Particle> getParticles() {
        return particles;
    }
}